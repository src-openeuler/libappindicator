Name: libappindicator
Version: 12.10.1
Release: 2
Summary: The library of application indicators
License: LGPL-2.1-or-later and GPL-3.0-or-later
URL: https://launchpad.net/libappindicator
Source0: https://launchpad.net/ubuntu/+archive/primary/+sourcefiles/%{name}/%{version}+20.10.20200706.1-0ubuntu1/%{name}_%{version}+20.10.20200706.1.orig.tar.gz
Patch0001: 0001_Fix_mono_dir.patch
BuildRequires:  autoconf automake libtool
BuildRequires:  pkgconfig(dbus-glib-1)
BuildRequires:  pkgconfig(dbusmenu-gtk-0.4) >= 0.5.90
BuildRequires:  pkgconfig(dbusmenu-gtk3-0.4) >= 0.5.90
BuildRequires:  pkgconfig(gio-2.0) >= 2.26
BuildRequires:  pkgconfig(gio-unix-2.0)
BuildRequires:  pkgconfig(glib-2.0) >= 2.35.4
BuildRequires:  pkgconfig(gobject-introspection-1.0) >= 0.10
BuildRequires:  pkgconfig(gtk+-2.0) >= 2.18
BuildRequires:  pkgconfig(gtk+-3.0) >= 2.91
BuildRequires:  /usr/bin/glib-genmarshal
BuildRequires:  /usr/bin/glib-mkenums
BuildRequires:  /usr/bin/valac
BuildRequires:  /usr/bin/vapigen
BuildRequires:  gtk-doc

%description
The libappindicator allows applications to export a menu into the Unity Menu bar.
It bases on KSNI and works in KDE and will fallback to generic Systray support if
none of those are available.

%package devel
Summary: Development package for libappindicator
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the development files for the appindicator library.

%package gtk3
Summary: Package of application indicators library - GTK 3

%description gtk3
This package contains the GTK 3 version of libappindicator.

%package gtk3-devel
Summary: Development package for libappindicator-gtk3
Requires: %{name}-gtk3 = %{version}-%{release} 

%description gtk3-devel
This package contains the development files for the appindicator-gtk3 library.

%package docs
Summary: Documentation package for libappindicator and libappindicator-gtk3
BuildArch: noarch
%description docs
This package contains the documentation for the appindicator and
appindicator-gtk3 libraries.

%prep
%autosetup -p1 -c %{name}-%{version}

sed -i "s#mono-csc#mcs#g" configure.ac
sed -i 's/--nogtkinit//' docs/reference/Makefile.am
gtkdocize --copy
cp -f gtk-doc.make gtk-doc.local.make
autoreconf -vif

%build
%global _configure ../configure
mkdir build-gtk2 build-gtk3
pushd build-gtk2
export CFLAGS="%{optflags} $CFLAGS -Wno-deprecated-declarations -Wno-error"
%configure --with-gtk=2 --enable-gtk-doc --disable-static
make -j1 V=1
popd
pushd build-gtk3
export CFLAGS="%{optflags} $CFLAGS -Wno-deprecated-declarations"
%configure --with-gtk=3 --enable-gtk-doc --disable-static
make -j1 V=1
popd

%install
%make_install -C build-gtk2
%make_install -C build-gtk3
%delete_la

%files
%license COPYING COPYING.LGPL.2.1
%doc AUTHORS README
%{_libdir}/libappindicator.so.*

%files devel
%dir %{_includedir}/libappindicator-0.1/
%dir %{_includedir}/libappindicator-0.1/libappindicator/
%{_includedir}/libappindicator-0.1/libappindicator/*.h
%{_libdir}/libappindicator.so
%{_libdir}/pkgconfig/appindicator-0.1.pc

%files gtk3
%license COPYING COPYING.LGPL.2.1
%doc AUTHORS README
%{_libdir}/libappindicator3.so.*
%{_libdir}/girepository-1.0/AppIndicator3-0.1.typelib

%files gtk3-devel
%dir %{_includedir}/libappindicator3-0.1/
%dir %{_includedir}/libappindicator3-0.1/libappindicator/
%{_includedir}/libappindicator3-0.1/libappindicator/*.h
%{_libdir}/libappindicator3.so
%{_libdir}/pkgconfig/appindicator3-0.1.pc
%{_datadir}/gir-1.0/AppIndicator3-0.1.gir
%{_datadir}/vala/vapi/appindicator3-0.1.vapi
%{_datadir}/vala/vapi/appindicator3-0.1.deps

%files docs
%doc %{_datadir}/gtk-doc/html/libappindicator/

%changelog
* Fri Jan 03 2025 Funda Wang <fundawang@yeah.net> - 12.10.1-2
- cleanup spec

* Wed Sep 6 2023 liyanan <thistleslyn@163.com> - 12.10.1-1
- Upgrade to version 12.10.1

* Wed Jan 20 2021 Ge Wang <wangge20@huawei.com> - 12.10.0-27
- Modify license information.

* Wed Oct 21 2020 chengzihan <chengzihan2@huawei.com> - 12.10.0-26
- Remove Subpackage python2-appindicator

* Fri Feb 07 2020 yanzhihua <yanzhihua4@huawei.com> - 12.10.0-25
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:ldconfig_scriptlets does not exist so modify the use of ldconfig, and rename the patch

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 12.10.0-24
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:optimization the spec

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 12.10.0-23
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:optimization the spec

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 12.10.0-22
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:optimization the spec

* Tue Nov 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 12.10.0-21
- Package init
